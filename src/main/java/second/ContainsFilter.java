package second;

public class ContainsFilter implements Filter {
    private String pattern;

    public ContainsFilter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String s) {
        return s.contains(pattern);
    }
}
