package second;

public interface Filter {
    boolean apply(String s);
}
