package second;

public class ContainsNumbersFilter implements Filter {

    @Override
    public boolean apply(String s) {
        for(char c: s.toCharArray()){
            if(c>='0' && c<='9'){
                return true;
            }
        }
        return false;
    }
}
