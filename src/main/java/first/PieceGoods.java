package first;

import java.util.Arrays;
import java.util.Objects;

public class PieceGoods extends Product{
    private double weight;

    public PieceGoods(String name, String[] description, double weight) {
        super(name, description);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PieceGoods that = (PieceGoods) o;
        return weight == that.weight && Objects.equals(this.getName(), that.getName()) &&
                Arrays.equals(this.getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return 31 * this.hashCode() + Objects.hash(weight);
    }
}
