package first;

import java.util.Arrays;

public class Сonsignment {
    private String[] description;
    private PackagedProduct[] products;

    public Сonsignment(String[] description, PackagedProduct ... packagedProducts) {
        this.description = description;
        this.products = packagedProducts;
    }

    public double getWeight(){
        double sum = 0;
        for(PackagedProduct product: products){
            sum+=product.getGrossWeight();
        }
        return sum;
    }

    public String[] getDescription() {
        return description;
    }

    public PackagedProduct[] getProducts() {
        return products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Сonsignment that = (Сonsignment) o;
        return Arrays.equals(description, that.description) &&
                Arrays.equals(products, that.products);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(description);
        result = 31 * result + Arrays.hashCode(products);
        return result;
    }

    @Override
    public String toString() {
        return "first.Сonsignment{" +
                "description=" + Arrays.toString(description) +
                ", products=" + Arrays.toString(products) +
                '}';
    }
}
