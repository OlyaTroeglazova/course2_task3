package first;

public interface PackagedProduct {
    double getNetWeight();
    double getGrossWeight();
    String getName();
}
