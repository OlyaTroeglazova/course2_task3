package first;

import java.util.Arrays;
import java.util.Objects;

public class Product {
     private String name;
     private String[] description;

    public Product(String name, String[] description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String[] getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Arrays.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name);
        result = 31 * result + Arrays.hashCode(description);
        return result;
    }

    @Override
    public String toString() {
        return "first.Product{" +
                "name='" + name + '\'' +
                ", description=" + Arrays.toString(description) +
                '}';
    }
}
