package first;

import java.util.Objects;

public class PackagedWeightProduct implements PackagedProduct{
    private WeightProduct product;
    private double weight;
    private ProductPackaging packaging;

    public PackagedWeightProduct(WeightProduct product, double weight, ProductPackaging packaging) {
        this.product = product;
        this.weight = weight;
        this.packaging = packaging;
    }

    public double getNetWeight() {
        return weight;
    }

    public double getGrossWeight() {
        return weight + packaging.getWeight();
    }

    @Override
    public String getName() {
        return product.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackagedWeightProduct that = (PackagedWeightProduct) o;
        return weight == that.weight &&
                Objects.equals(product, that.product) &&
                Objects.equals(packaging, that.packaging);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, weight, packaging);
    }

    @Override
    public String toString() {
        return "first.PackagedWeightProduct{" +
                "product=" + product +
                ", weight=" + weight +
                ", packaging=" + packaging +
                '}';
    }
}
