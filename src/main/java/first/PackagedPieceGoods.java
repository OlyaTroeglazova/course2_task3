package first;

import java.util.Objects;

public class PackagedPieceGoods implements PackagedProduct {
    private PieceGoods product;
    private int count;
    private ProductPackaging packaging;

    public PackagedPieceGoods(PieceGoods product, int count, ProductPackaging packaging) {
        this.product = product;
        this.count = count;
        this.packaging = packaging;
    }

    public double getNetWeight() {
        return product.getWeight()*count;
    }

    public double getGrossWeight() {
        return this.getNetWeight()+packaging.getWeight();
    }

    @Override
    public String getName() {
        return product.getName();
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackagedPieceGoods that = (PackagedPieceGoods) o;
        return count == that.count &&
                Objects.equals(product, that.product) &&
                Objects.equals(packaging, that.packaging);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, count, packaging);
    }

    @Override
    public String toString() {
        return "first.PackagedPieceGoods{" +
                "product=" + product +
                ", count=" + count +
                ", packaging=" + packaging +
                '}';
    }
}
