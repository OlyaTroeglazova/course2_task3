package third;

import first.PackagedProduct;
import first.Сonsignment;
import second.Filter;

public class ProductService {
    public int countByFilter(Сonsignment consignment, Filter filter){
        int count = 0;
        for(PackagedProduct p: consignment.getProducts()){
            if(filter.apply(p.getName())){
                count++;
            }
        }
        return count;
    }
}
