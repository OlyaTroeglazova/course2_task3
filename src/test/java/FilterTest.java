import org.junit.Assert;
import org.junit.Test;
import second.BeginStringFilter;
import second.ContainsFilter;
import second.ContainsNumbersFilter;

public class FilterTest {
    @Test
    public void beginStringTest(){
        String s = "Мама мыла раму";
        BeginStringFilter beginStringFilter1 = new BeginStringFilter("Мама");
        BeginStringFilter beginStringFilter2 = new BeginStringFilter("мыла");

        Assert.assertTrue(beginStringFilter1.apply(s));
        Assert.assertFalse(beginStringFilter2.apply(s));
    }

    @Test
    public void containsTest(){
        String s = "Мама мыла раму";
        ContainsFilter containsFilter1 = new ContainsFilter("Мама");
        ContainsFilter containsFilter2 = new ContainsFilter("Папа");

        Assert.assertTrue(containsFilter1.apply(s));
        Assert.assertFalse(containsFilter2.apply(s));
    }

    @Test
    public void containsNumbersTest(){
        String s1 = "Мама мыла 1 раму";
        String s2 = "Мама мыла раму";
        ContainsNumbersFilter containsNumbersFilter = new ContainsNumbersFilter();

        Assert.assertTrue(containsNumbersFilter.apply(s1));
        Assert.assertFalse(containsNumbersFilter.apply(s2));
    }

}
