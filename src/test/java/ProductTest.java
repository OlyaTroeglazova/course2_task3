import first.*;
import org.junit.Assert;
import org.junit.Test;

public class ProductTest {

    @Test
    public void ProductTest(){
        Product product1 = new Product("имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        Product product2 = new Product("имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        Product product3 = new Product("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"});

        Assert.assertEquals(product1.getName(), "имя товара");
        Assert.assertEquals(product1.getDescription(), new String[]{"описание 1", "описание 2", "описание 3"});
        Assert.assertTrue(product1.equals(product2));
        Assert.assertFalse(product1.equals(product3));
    }

    @Test
    public void WeightProductTest(){
        WeightProduct product1 = new WeightProduct("имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        WeightProduct product2 = new WeightProduct("имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        WeightProduct product3 = new WeightProduct("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"});

        Assert.assertEquals(product1.getName(), "имя товара");
        Assert.assertEquals(product1.getDescription(), new String[]{"описание 1", "описание 2", "описание 3"});
        Assert.assertTrue(product1.equals(product2));
        Assert.assertFalse(product1.equals(product3));
    }

    @Test
    public void PieceGoodsTest(){
        PieceGoods product1 = new PieceGoods("имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 3.);
        PieceGoods product2 = new PieceGoods("имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 3.);
        PieceGoods product3 = new PieceGoods("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 3.);

        Assert.assertEquals(product1.getName(), "имя товара");
        Assert.assertEquals(product1.getDescription(), new String[]{"описание 1", "описание 2", "описание 3"});
        Assert.assertEquals(product1.getWeight(), 3., 1e-10);
        Assert.assertTrue(product1.equals(product2));
        Assert.assertFalse(product1.equals(product3));
    }

    @Test
    public void ProductPackagingTest(){
        ProductPackaging packaging1 = new ProductPackaging("название упаковки", 0.2);
        ProductPackaging packaging2 = new ProductPackaging("название упаковки", 0.2);
        ProductPackaging packaging3 = new ProductPackaging("другое название упаковки", 0.2);

        Assert.assertEquals(packaging1.getName(), "название упаковки");
        Assert.assertEquals(packaging1.getWeight(), 0.2, 1e-10);
        Assert.assertTrue(packaging1.equals(packaging2));
        Assert.assertFalse(packaging1.equals(packaging3));
    }

    @Test
    public void PackagedWeightProductTest(){
        WeightProduct product1 = new WeightProduct("имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        WeightProduct product2 = new WeightProduct("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        ProductPackaging packaging = new ProductPackaging("название упаковки", 0.2);

        PackagedWeightProduct packagedWeightProduct1 = new PackagedWeightProduct(product1, 10., packaging);
        PackagedWeightProduct packagedWeightProduct2 = new PackagedWeightProduct(product2, 15., packaging);

        Assert.assertEquals(packagedWeightProduct1.getNetWeight(), 10., 1e-10);
        Assert.assertEquals(packagedWeightProduct1.getGrossWeight(), 10.+packaging.getWeight(), 1e-10);
        Assert.assertFalse(packagedWeightProduct1.equals(packagedWeightProduct2));
    }

    @Test
    public void PackagedPieceGoodsTest(){
        PieceGoods product1 = new PieceGoods("имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 2.);
        PieceGoods product2 = new PieceGoods("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 4.);
        ProductPackaging packaging = new ProductPackaging("название упаковки", 0.2);

        PackagedPieceGoods packagedPieceGoods1 = new PackagedPieceGoods(product1, 10, packaging);
        PackagedPieceGoods packagedPieceGoods2 = new PackagedPieceGoods(product2, 15, packaging);

        Assert.assertEquals(packagedPieceGoods1.getNetWeight(), 20., 1e-10);
        Assert.assertEquals(packagedPieceGoods1.getGrossWeight(), 20.+packaging.getWeight(), 1e-10);
        Assert.assertFalse(packagedPieceGoods1.equals(packagedPieceGoods2));
    }

    @Test
    public void ConsignmentTest(){
        PieceGoods product1 = new PieceGoods("имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 2.);
        WeightProduct product2 = new WeightProduct("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"});
        ProductPackaging packaging = new ProductPackaging("название упаковки", 0.2);
        PackagedPieceGoods packagedPieceGoods = new PackagedPieceGoods(product1, 10, packaging);
        PackagedWeightProduct packagedWeightProduct = new PackagedWeightProduct(product2, 10., packaging);

        Сonsignment consignment = new Сonsignment(new String[]{"Описание", "описание"}, packagedPieceGoods, packagedWeightProduct);

        Assert.assertEquals(consignment.getDescription(), new String[]{"Описание", "описание"});
        Assert.assertEquals(consignment.getWeight(), packagedPieceGoods.getGrossWeight()+packagedWeightProduct.getGrossWeight(), 1e-10);
    }

}
