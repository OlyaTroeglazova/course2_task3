import first.*;
import org.junit.Assert;
import org.junit.Test;
import second.BeginStringFilter;
import second.ContainsFilter;
import second.ContainsNumbersFilter;
import third.ProductService;

public class ProductServiceTest {
    @Test
    public void serviceTest(){
        WeightProduct product1 = new WeightProduct("имя товара 1", new String[]{"описание 1", "описание 2", "описание 3"});
        WeightProduct product2 = new WeightProduct("имя товара 2", new String[]{"описание 1", "описание 2", "описание 3"});
        PieceGoods product3 = new PieceGoods("имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 2.);
        PieceGoods product4 = new PieceGoods("другое имя товара", new String[]{"описание 1", "описание 2", "описание 3"}, 4.);

        ProductPackaging packaging1 = new ProductPackaging("название упаковки", 0.2);
        ProductPackaging packaging2 = new ProductPackaging("название упаковки", 0.2);

        PackagedWeightProduct packagedWeightProduct1 = new PackagedWeightProduct(product1, 10., packaging1);
        PackagedWeightProduct packagedWeightProduct2 = new PackagedWeightProduct(product2, 15., packaging1);
        PackagedPieceGoods packagedPieceGoods1 = new PackagedPieceGoods(product3, 10, packaging2);
        PackagedPieceGoods packagedPieceGoods2 = new PackagedPieceGoods(product4, 15, packaging2);

        Сonsignment consignment = new Сonsignment(new String[]{"описание"}, packagedPieceGoods1, packagedPieceGoods2, packagedWeightProduct1, packagedWeightProduct2);

        ProductService service = new ProductService();
        BeginStringFilter beginStringFilter = new BeginStringFilter("имя");
        ContainsFilter containsFilter = new ContainsFilter("другое");
        ContainsNumbersFilter containsNumbersFilter = new ContainsNumbersFilter();

        Assert.assertEquals(service.countByFilter(consignment, beginStringFilter), 3);
        Assert.assertEquals(service.countByFilter(consignment, containsFilter), 1);
        Assert.assertEquals(service.countByFilter(consignment, containsNumbersFilter), 2);
    }
}
